//
//  Characters.swift
//  AppIdine
//
//  Created by Munabeno on 22/01/2020.
//  Copyright © 2020 Munabeno. All rights reserved.
//

import SwiftUI

struct CharacterSection: Codable, Identifiable {
    var id:UUID
    var name:String
    var characters:[Character]
}

struct Character: Codable, Equatable, Identifiable{
    
    var id:UUID
    var name:String
    var force:Int
    var spirituel:Int
    var endurance:Int
    var defensePhysique:Int
    var defenseMagique:Int
    var vitesse:Int
    
    var mainImage: String {
        name.replacingOccurrences(of: " ", with: "-").lowercased()
    }

    var thumbnailImage: String {
        "\(mainImage)-thumb"
    }
    
    #if DEBUG
    static let exemple = Character(
        id: UUID(),
        name: "Deku",
        force: 100,
        spirituel: 10,
        endurance: 10,
        defensePhysique: 100,
        defenseMagique: 100,
        vitesse: 10
    )
    #endif
    
}
